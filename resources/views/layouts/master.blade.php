
<!DOCTYPE html>
<html>
	<head>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<title>Multimedia site</title>
		{{Html::style('css/bootstrap/bootstrap.css')}}
		{{Html::style('css/custom.css')}}
		{!! Html::style('css/datatables/dataTables.bootstrap.css') !!}
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	</head>
	<body>
	<div class= "wraper">
		@include('layouts._navbar')
		<div class="page-title">
			@yield('title')
		</div>
		@if(Request::url() == url('/stats/video') || Request::url() == url('/stats/image') || Request::url() == url('/stats/music') )
			<div class="sidebar">
				@include('layouts._sidebar')
			</div>
		@endif
		<?php if(strpos(Request::url(), "profile")) { ?>
			<div class="sidebar">
				@include('layouts._sidebar-profile')
			</div>
		<?php } ?>
		<?php if(strpos(Request::url(), "message")) { ?>
			<div class="sidebar">
				@include('layouts._sidebar-messages')
			</div>
		<?php } ?>
		
		<div class="page-content">
			@yield('content')
		</div>
		<div class="page-footer">
			@yield('footer')
		</div>
	</div>
	
	
	{{Html::script('js/bootstrap/bootstrap.min.js')}}
	
	{!! Html::script('js/datatables/jquery.dataTables.js') !!}
	{!! Html::script('js/datatables/dataTables.bootstrap.js') !!}
	{!! Html::script('js/datatables/dataTables.tableTools.js') !!}
	
	</body>

</html>
