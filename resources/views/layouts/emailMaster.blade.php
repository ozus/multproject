<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		{{ Html::style('css/email.css') }}
	</head>
	<body class="mail-body">
		<div class="mail-content">
			@yield('content')
		</div>
		<div class="mail-footer">
			@yield('footer')
		</div>	
	</body>
	
</html>
