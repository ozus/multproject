
@extends('layouts.master')

@section('title')
Images
@endsection

@section('content')
<style>
.carousel-inner > .item > img {
  width:1280px;
  height:720px;
  margin: 0 auto;
}
</style>
<div class = container>
	<?php 
		$n = $images -> count();
	?> 
	<div class="row">
		<button type="button" style="float: right;" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Gallery</button>
	</div>
	
	
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Gallery</h4>
				</div>
				<div class="modal-body">
				
					
					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							@for($i = 0; $i < $n; $i++)
								@if($i == 0)
									<li data-target="#myCarousel" data-slide-to="{{$i}}" class="active"></li>
								@else
						    		<li data-target="#myCarousel" data-slide-to="{{$i}}"></li>
						    	@endif
						    @endfor
						</ol>
						<div class="carousel-inner" role="listbox">
							@for($i = 0; $i < $n; $i++)
								@if($i == 0)
								<div class="item active">
									<img src="{{url('/')}}/large/<?php echo $images[$i] -> name . "." . $images[$i] -> extention; ?>"  />
								</div>
								@else
								<div class="item">
									<img src="{{url('/')}}/large/<?php echo $images[$i] -> name . "." . $images[$i] -> extention; ?>"  />
								</div>
								@endif
							@endfor
						</div>
						
						  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
						    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
						    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						
					</div>
					
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	@if(count($errors) > 0)
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
					
					<ul>
						@foreach($errors -> all() as $error)
							<li>{{$error}}</li>
						@endforeach	
					</ul>	
					
			</div>
		</div>
	@endif	
	@if(session('success'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success">
				{{session('success')}}
			</div>
		</div>
	@endif
	@if(session('error'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
				{{session('error')}}
			</div>
		</div>
	@endif
	
	<div class="row">
		<div class="col-md-2">
			<label>Items per page:</label>
			<div class="form-group">
				<select id="pagi" class="form-control" name="pag" onchange="cngPag(this.value);">
					<option>Items per page</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
				</select>
			</div>
		</div>
		<div id="demo"></div>
	</div>
	<div class="row">
		<div class="col-md-8 col-lg-8 col-sm-12">
			@foreach($images as $image)
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-10 block-wraper">
						<div class="row title-wraper">
							<span>{{$image -> user_name }}</span>
							<span style="float: right;">
								@if(Auth::check() && Auth::user() -> name == $image -> user)
									<a href="/user/profile/info/<?php echo Auth::user() -> id; ?>" >{{$image -> user}}</a>
								@else
									{{$image -> user}}
								@endif
							</span>
						</div>
						<div class="row">
							<div class="col-md-6 col-lg-6 col-sm-6">
								<a href="{{url('/')}}/showImage/{{$image -> id}}"><img src="{{url('/')}}/images/<?php echo $image -> name . "." . $image -> extention; ?>" width="350" height="350" /></a>   
							</div>
							<div class="col-md-6 col-lg-6 col-sm-6 desc-wraper">
								{{$image -> desc}}
							</div>			
						</div>
						<div class="row">
							<span><a href="/download/<?php echo $image -> id; ?>" class="btn btn-default">Download</a></span>
							@if(Auth::check())
								@if(Auth::user() -> name == $image -> user)
									<span style="float: right;"><a href="/user/edit/<?php echo $image -> id; ?>" class="btn btn-default">Edit</a></span>
									<span style="float: right;"><button type="button" value="<?php echo $image -> id; ?>" name="<?php echo $image -> user_name; ?>" class="btn btn-default" onclick="delConfrm(this.value, this.name);">Delete</button></span>	
								@endif
							@endif
						</div>
					</div>
				</div>
				
				<br>
			@endforeach
			<div class="row">
					{{$images -> links()}}
					{{$images -> total()}}
			</div>
		</div>
		<div class="col-md-4 col-lg-4 col-sm-12 col-md-push-2 col-lg-push-2 col-sm-push-1">
			<div class="row">
				
				<input type="radio" name="preference" value="viewed" checked="checked" onclick="al()">Most viewed
				<input type="radio" name="preference" value="liked" onclick="al()">Most liked
				<input type="radio" name="preference" value="disliked" onclick="al()">Most disliked
				@if(Auth::check())
					<input type="radio" name="preference" value="favorites" onclick="al()">My favorites
				@endif
				
			</div>
			<div class="row">
				<label id="prefLbl">Most viewed</label>
			</div>
			<div id="pref" class="row">
				
				@foreach($viewed as $view)
					
						<div class="row">
								{{$view -> user_name}}
						</div>
						<div class="row">
							{{$view -> path}}
						</div>
	
					<br>
				@endforeach
			</div>
		</div>	
	</div>
	
</div>	

<script>

function delConfrm(id, name) {

	
	
	 var y = confirm("Are you sure you want to delete file: " + name);
	 var type = "image";
	if(y == true) {
		//document.getElementById('del').innerHTML = "Video delited";
		window.location.assign("{{route('file.delete')}}" + "/" + id + "/" + type);

	}
}

	function cngPag(pag) {

		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if(this.readyState == 4 && this.status == 200) {
				document.body.innerHTML = "";
				document.body.innerHTML = this.responseText
				
				
			}
		}
		xhttp.open("GET", "{{route('ajax.image')}}" + "/" + pag, true);	
		xhttp.send();
	}


	function al() {

		var pref = document.getElementsByName('preference');
		var lng = pref.length;
		for(var i = 0; i < lng; i++) {

			//var value= pref[i].value;
			
			if(pref[i].checked) {

				var value = pref[i].value

				var lbl = document.getElementById('prefLbl');
				lbl.innerHTML = "";
				lbl.innerHTML = "Most " + value;
				
				var xhttp = new XMLHttpRequest();

				xhttp.onreadystatechange = function() {
					if(this.readyState == 4 && this.status == 200) {
						var pr = document.getElementById('pref');
						pr.innerHTML = "";
						pr.innerHTML = this.responseText; 
					}
				}
				xhttp.open("GET", "{{route('image.preference')}}" + "/" + value, true);
				xhttp.send();
				
			}
		}	
		
	}
</script>	

@endsection



















