@extends('layouts.master')

@section('title')
Music
@endsection

@section('content')
<div class="container">
	@if(count($errors) > 0)
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
					
					<ul>
						@foreach($errors -> all() as $error)
							<li>{{$error}}</li>
						@endforeach	
					</ul>	
					
			</div>
		</div>
	@endif	
	@if(session('success'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success">
				{{session('success')}}
			</div>
		</div>
	@endif
	@if(session('error'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
				{{session('error')}}
			</div>
		</div>
	@endif
	
	<div class="row">
		<div class="col-md-2">
			<label>Items per page:</label>
			<div class="form-group">
				<select id="pagi" class="form-control" name="pag" onchange="cngPag(this.value);">
					<option>Items per page</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
				</select>
			</div>
		</div>
		<div id="demo"></div>
		<div id="demo1"></div>
	</div>
	<div class="row">
		<div class="col-md-8 col-lg-8 col-sm-8">
			@foreach($musics as $music)
				<div class="row">
					<div class="col-md-12 col-lg-12 col-sm-12 block-wraper">
						<div class="row title-wraper">
							<span>{{$music -> user_name}}</span>
							<span style="float: right;">
								@if(Auth::check() && Auth::user() -> name == $music -> user)
									<a href="{{url('/')}}/user/profile/info/<?php echo Auth::user() -> id; ?>" >{{$music -> user}}</a>
								@else
									{{$music -> user}}
								@endif
							</span>
						</div>
						<div class="row">
							{{$music -> path }}
						</div>
						<div class="row desc-wraper">
							{{$music -> desc}}
						</div>
						<div class="row">
							<span><a href="{{url('/')}}/download/<?php echo $music -> id; ?>" class="btn btn-default">Download</a></span>
							<span><a href="{{url('/')}}/showMusic/<?php echo $music -> id; ?>" class="btn btn-default">Show</a></span>
							@if(Auth::check())
								@if(Auth::user() -> name == $music -> user)
									<span style="float: right;"><a href="{{url('/')}}/user/edit/<?php echo $music-> id; ?>" class="btn btn-default">Edit</a></span>
									<span style="float: right;"><button type="button" value="<?php echo $music -> id; ?>" name="<?php echo $music -> user_name; ?>" class="btn btn-default" onclick="delConfrm(this.value, this.name);">Delete</button></span>								@endif
							@endif
						</div>
					</div>
				</div>
				<br>
			@endforeach
			<div class="row">
				{{$musics -> links()}}
				{{$musics -> total()}}
			</div>
		</div>
		<div class="col-md-4 col-lg-4 col-sm-4 col-md-push-2 col-lg-push-2 col-sm-push-1">
			<div class="row">
				
				<input type="radio" name="preference" value="viewed" checked="checked" onclick="al()">Most viewed
				<input type="radio" name="preference" value="liked" onclick="al()">Most liked
				<input type="radio" name="preference" value="disliked" onclick="al()">Most disliked
				@if(Auth::check())
					<input type="radio" name="preference" value="favorites" onclick="al()">My favorites
				@endif
				
			</div>
			<div class="row">
				<label id="prefLbl">Most viewed</label>
			</div>
			<div id="pref" class="row">
				
				@foreach($viewed as $view)
					
						<div class="row">
								{{$view -> user_name}}
						</div>
						<div class="row">
							{{$view -> path}}
						</div>
	
					<br>
				@endforeach
			</div>
		</div>
	</div>
	
</div>

<script>

function delConfrm(id, name) {

	
	
	 var y = confirm("Are you sure you want to delete file: " + name);
	 var type = "video";
	if(y == true) {
		//document.getElementById('del').innerHTML = "Video delited";
		window.location.assign("{{route('file.delete')}}" + "/" + id + "/" + type);

	}
}

	function cngPag(pag) {

		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if(this.readyState == 4 && this.status == 200) {
				document.body.innerHTML = "";
				document.body.innerHTML = this.responseText
				
				
			}
		}
		xhttp.open("GET", "{{route('ajax.music')}}" + "/" + pag, true);	
		xhttp.send();
	}

	function al() {

		var pref = document.getElementsByName('preference');
		var lng = pref.length;
		for(var i = 0; i < lng; i++) {

			//var value= pref[i].value;
			
			if(pref[i].checked) {

				var value = pref[i].value

				var lbl = document.getElementById('prefLbl');
				lbl.innerHTML = "";
				lbl.innerHTML = "Most " + value;
				
				var xhttp = new XMLHttpRequest();

				xhttp.onreadystatechange = function() {
					if(this.readyState == 4 && this.status == 200) {
						var pr = document.getElementById('pref');
						pr.innerHTML = "";
						pr.innerHTML = this.responseText; 
					}
				}
				xhttp.open("GET", "{{route('music.preference')}}" + "/" + value, true);
				xhttp.send();
				
			}
		}	
		
	}
</script>




@endsection
