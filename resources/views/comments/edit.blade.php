@extends('layouts.master')

@section('title')
Edit Comment
@endsection

@section('content')


<div class="container">
	@if(count($errors) > 0)
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
					
					<ul>
						@foreach($errors -> all() as $error)
							<li>{{$error}}</li>
						@endforeach	
					</ul>	
					
			</div>
		</div>
	@endif	
	@if(session('success'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-success">
				{{session('success')}}
			</div>
		</div>
	@endif
	@if(session('error'))
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 alert alert-danger">
				{{session('error')}}
			</div>
		</div>
	@endif
	
	
	<div class="row">
		<div class="col-md-12 col-lg-12 col-sm-12">
			<form action="/comment/edit" method="post">
				<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
				<input type="hidden" name="id" value="<?php echo $id; ?>" />
				<div class="form-group">
					<label>Edit Comment:</label>
					<textarea class="form-control" name="comment" rows="10" cols="10">{{$comm[0] -> comment}}</textarea>
				</div>
				<input type="submit" class="btn btn-default" value="Update" />
			</form>
		</div>
	</div>
	
</div>


@endsection