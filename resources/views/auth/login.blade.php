@extends('layouts.authMaster')
	
		@section('title')
		Log in
		@endsection
		@section('content')
		@if(count($errors) > 0)
			<ul class="alert alert-danger">
			@foreach($errors -> all() as $error)
				<li>{{$error}}</li>
			@endforeach
			</ul>
		@endif
		<form name="login" action="/auth/login" method="post" >
			<input type="hidden" name="_token" value="<?php echo csrf_token() ?>" >
			<div class="form-group">
				<label>Email:</label>
				<input type="text" class="form-control" name="email" />
			</div>
			<div class="form-goup">
				<label>Password:</label>
				<input type="text" class="form-control" name="password" />
			</div>
			<div class="checkbox">
				<label><input type="checkbox" name="remember" value="remember_me" >Remember me</label> <span><a href="{{url('/auth/email')}}">Forgot password</a></span>
			</div>
		</form>
		@endsection
		@section('footer')
		<div class="form-group">
			<button type="submit" class="auth-btn" onclick="submitForm();">Login</button>
		</div>
		<button type="button" class="auth-btn" onclick="returnHome();">Return</button>
		
		
		<script>
			function submitForm() {
				document.login.submit();
			}
			function returnHome() {
				window.location.assign("{{route('home')}}");
			}
		</script>
		@endsection