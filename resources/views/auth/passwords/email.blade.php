<html>
	<head>
		<title>Reset password email</title>
	</head>
	<body>
		@if(count($errors) >0)
			<ul>
				@foreach($errors -> all() as $error)
					<li>{{$error}}</li>
				@endforeach
			</ul>
		@endif
		{{session('status')}}
		<h3>Password reset</h3><br>
		Ples enter you email in field belov and we will send you eamil with password reset instructions.<br>
		<form name="form" action="/auth/email" method="post">
		{{ csrf_field() }}
			<input type="email" name="email" /><br>
			<input type="submit" value="Send" />
		</form>
	</body>
</html>