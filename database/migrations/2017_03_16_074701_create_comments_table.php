<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            
            $table -> integer('usr_id') -> unsigned();
            $table -> foreign('usr_id') -> references('id') -> on('users') -> onDelete('cascade');
            
            $table -> string('comment', 1000) -> nullable();
            $table -> string('author', 255) -> unique();
            $table -> time('time_posted');
            $table -> date('date_posted');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
