<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use Auth;

class MusicController extends Controller
{
    
	
	public function getMusic() {
		 
		$musics = File::where('path', 'LIKE', '%music%') -> paginate(3);
		 
		$viewed = File::where('path', 'LIKE', '%music%') -> where('viewed', '>', '5') -> limit(5) -> get();
	
		return view('pages.music', ['musics'=> $musics, 'viewed' => $viewed]);
	
	}
	
	public function ajaxMusic($pag = null) {
	
		$musics = File::where('path', 'LIKE', '%music%') -> paginate($pag);
	
		$viewed = File::where('path', 'LIKE', '%music%') -> where('viewed', '>', '5') -> limit(5) -> get();
	
		return view('pages.ajax.musics', ['musics' => $musics, 'viewed' => $viewed ]);
	
	}
	
	public function showMusic($id = null) {
	
		$music = File::where('id', '=', $id) -> get();
	
		$music[0] -> viewed = $music[0] -> viewed + 1;
	
		$music[0] -> save();
		
		
		
		$viewed = File::where('path', 'LIKE', '%music%') -> where('viewed', '>', '5') -> orderBy('viewed') -> limit(5) -> get();
		
		$files = File::where('path', 'LIKE', '%music%') -> get();
		
		$filesFiltered = $files -> filter(function($value, $key) use ($id) {
			return $value -> id == $id;
		});
		
		$musics = $filesFiltered -> flatten();
		//$musics = File::where('id', '=', $id) -> get();
	
		return view('pages.showMsc', ['musics' => $musics, 'viewed' => $viewed, 'files' => $files, 'chck' => 0]);
	
	}
	
	public function ajaxLike($id = null) {
	
		//return "ID = " . $id;
	
		$like = File::where('id', '=', $id) -> get();
	
		$like[0] -> liked = $like[0] -> liked + 1;
	
		$like[0] -> save();
	
		echo "<button type='button' id='unlikeButt' class='btn btn-default' onclick='unLike();'>Unlike video</button>";
		echo "  ";
		echo "Song liked";
	
	
	
	}
	
	
	
	public function ajaxUnlike($id = null) {
	
		//return "ID = " . $id;
	
		$like = File::where('id', '=', $id) -> get();
	
		$like[0] -> liked = $like[0] -> liked - 1;
	
		$like[0] -> save();
	
		echo "<button type='button' id='likeButt' class='btn btn-default' onclick='like();'>Like video</button>";
	
	
	}
	
	
	
	public function ajaxDislike($id = null) {
	
		//return "ID = " . $id;
	
		$dislike = File::where('id', '=', $id) -> get();
	
		$dislike[0] -> disliked = $dislike[0] -> disliked + 1;
	
		$dislike[0] -> save();
	
		echo "<button type='button' id='unDislikeButt' class='btn btn-default' onclick='unDislike();'>Remove dislike</button>";
		echo "  ";
		echo "Song disliked";
	
	
	}
	
	
	public function ajaxUndislike($id = null) {
	
		//return "ID = " . $id;
	
		$dislike = File::where('id', '=', $id) -> get();
	
		$dislike[0] -> disliked = $dislike[0] -> disliked - 1;
	
		$dislike[0] -> save();
	
		echo "<button type='button' id='dislikeButt' class='btn btn-default' onclick='dislike();'>Dislike video</button>";
	
	}
	
	
	public function ajaxPref($value = null) {
	
		if($value == "viewed") {
			$title = "Most viewed";
			$viewed = File::where('path', 'LIKE', '%music%') -> where('viewed', '>', '5') -> orderBy('viewed') -> limit(5) -> get();
			return view('pages.ajax.videoPref', ['viewed' => $viewed]);
				
		}
	
		if($value == "liked") {
			$title = "Most Liked";
			$viewed = File::where('path', 'LIKE', '%music%') -> where('liked', '>', '1') -> orderBy('liked') -> limit(5) -> get();
				
			return view('pages.ajax.videoPref', ['viewed' => $viewed]);
		}
	
		if($value == "disliked") {
			$title = "Most Disliked";
			$viewed = File::where('path', 'LIKE', '%music%') -> where('disliked', '>', '1') -> orderBy('disliked')-> limit(5) -> get();
			return view('pages.ajax.videoPref', ['viewed' => $viewed]);
		}
	
		if($value == 'favorites') {
				
			if(Auth::check()) {
					
				$viewed = Auth::user() -> favorits;
					
				$filteredFavs = $viewed -> filter(function($value, $key) {
					return str_contains($value -> path, 'music');
				});
						
					$return = $filteredFavs -> take(1);
						
					return view('pages.ajax.videoPref', ['viewed' => $return]);
						
			}
		}
	
	}
	
	
	public function ajaxNext($id = null) {
		//$idd == $id;
		$files = File::where('path', 'LIKE', '%music%') -> get();
		$viewed = File::where('path', 'LIKE', '%music%') -> where('viewed', '>', '5') -> limit(5) -> get();
		
		$filesFiltered = $files -> filter(function($value, $key) use ($id) {
			return $value -> id == $id;
		});
		
		
			$keys = $filesFiltered -> keys();
			$key = $files -> search($filesFiltered[$keys[0]]);
			
			
			$lng = count($files);
			if($key == $lng - 1) {
				$fileReturn = $files -> get($key);
			} else {
				$fileReturn = $files -> get($key + 1);
			}
		
			$array[0] = $fileReturn;
		
			$col = collect($array);
		
			return view('pages.showMsc', ['musics' => $col, 'viewed' => $viewed, 'files' => $files]); 
			//return redirect() -> route('show.music', ['id' => $col[0] -> id]);
		
	}
	
	public function ajaxPrev($id = null) {
		
		$files = File::where('path', 'LIKE', '%music%') -> get();
		$viewed = File::where('path', 'LIKE', '%music%') -> where('viewed', '>', '5') -> limit(5) -> get();
		
		$filesFiltered = $files -> filter(function($value, $key) use ($id) {
			return $value -> id == $id;
		});
		
		
			$keys = $filesFiltered -> keys();
			$key = $files -> search($filesFiltered[$keys[0]]);
				
				
			$lng = count($files);
			if($key == 0) {
				$fileReturn = $files -> get($key);
			} else {
				$fileReturn = $files -> get($key - 1);
			}
		
			$array[0] = $fileReturn;
		
			$col = collect($array);
		
			return view('pages.showMsc', ['musics' => $col, 'viewed' => $viewed, 'files' => $files]);
		
	}
	
}





















