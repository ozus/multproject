<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File as Fil;
use App\Favorit;
use Auth;
use App\User;

class UserController extends Controller
{
   
	public function editFile($id = null) {
	
		$fileData = Fil::select('desc', 'user_name') -> where('id', '=', $id) -> get();
	
		return view('usrpages.edit', ['data' => $fileData, 'id' => $id]);
	
	}
	
	public function postEditFile(Request $request) {
		if($request -> has('name') && $request -> has('desc')) {
			$newName = $request -> input('name');
			$newDesc = $request -> input('desc');
			$id = $request -> input('id');
				
			Fil::where('id', '=', $id) -> update(['user_name' => $newName, 'desc' => $newDesc]);
				
			$request -> session() -> flash('success', 'File successfuly updated');
			return back();
		} else {
			$request -> session() -> flash('error', 'File could not be updated');
			return back();
		}
	
	}
	
	
	public function ajaxAddFavorite($id = null) {
		
		$file = Fil::where('id', '=', $id) -> get();
		
		if(Auth::check()) {
		
			$favorite = new Favorit();
		
			$favorite -> path = $file[0] -> path;
			$favorite -> name = $file[0] -> name;
			$favorite -> extention = $file[0] -> extention;
			$favorite -> desc = $file[0] -> desc;
			$favorite -> user_name = $file[0] -> user_name;
			$favorite -> user = $file[0] -> user;
			$favorite -> liked = $file[0] -> liked;
			$favorite -> vewed = $file[0] -> viewed;
			$favorite -> disliked = $file[0] -> disliked;
		
			if(!Favorit::where('user_name', '=', $file[0] -> user_name) ->exists()) {
				Auth::user() -> favorits() -> save($favorite);
			} else {
				$fav = Favorit::where('user_name', '=', $file[0] -> user_name) -> get();
				Auth::user() -> favorits() -> attach($fav[0] -> id);
			}
			$butt = "<button type='button' id='removeFavButt' class='btn btn-default' onclick='remFav();'>Remove from favorites</button>";
				
		
			$array = array('first' => $butt, 'second' => 'Added to favorites');
			return json_encode($array);
				
		
		}
	
	}
	
	
	public function ajaxRemFavorite($id = null) {
		
		$file = Fil::where('id', '=', $id) -> get();
		
		if(Favorit::where('user_name', '=', $file[0] -> user_name) -> exists()) {
				
			$favorit = Favorit::where('user_name', '=', $file[0] -> user_name) -> get();
				
		} 
		
		if(Auth::check()  && $favorit ) {
				
			Auth::user() -> favorits() -> detach($favorit[0] -> id);
			if($favorit[0] -> users -> count() == 0) {	
				Favorit::where('id', '=', $favorit[0] -> id) -> delete(); 
			}
			
			$butt = "<button type='button' id='favButt' class='btn btn-default' onclick='addFav();'>Add to favorits</button>";
			
			$array = array('first' => $butt, 'second' => 'Removed form favorites');
			
			return json_encode($array);
				
		} else {
			
			$butt1 = "<button type='button' id='removeFavButt' class='btn btn-default' onclick='remFav();'>Remove from favorites</button>";
				
			$array1 = array('first' => $butt1, 'second' => 'Favorit could not be removed, something went wrong.');
			
			return  json_encode($array1);
		}
		
	}
	
	
	public function ajaxGradeFile($id = null, $type = null, $value = null) {
		
		//return "type: " . $type . " of " . $id . " with grade " . $value;
		
		$grade = Fil::where('id', '=', $id) -> get();
		
		$graded = $grade[0] -> graded + 1;
		$grade_sum = $grade[0] -> grade_sum + $value;
		
		$avrg_grade = $grade_sum/$graded;
		
		
		
		$grade[0] -> grade_sum = $grade_sum;
		$grade[0] -> graded = $graded;
		$grade[0] -> avrg_grade = $avrg_grade;
		
		$grade[0] -> save();
		
		return "You grade file " . $grade[0] -> user_name . " with grade  " . $value;
		
	}
	
	
	public function profile($id = null) {
		$user_data = User::where('id', '=', $id) -> get();
		return view('usrpages.profile-index', ['user' => $user_data]);
	}
	
	
	public function profilePostInfo(Request $request) {
		
		if($request -> has('password')) {
			$hashed_pass = bcrypt($request -> input('password'));
			
			User::where('id', '=', $request -> input('id')) -> update([
					'password' => $request -> input('password'),
					'hashed_password' => $hashed_pass
			]);
		}
		
		User::where('id', '=', $request -> input('id')) -> update([
				'name' => $request -> input('name'),
				'lname' => $request -> input('lname'),
				'city' => $request -> input('city'),
				'county' => $request -> input('country')
		]);
		
		if($request -> hasFile('profpic'))  {
			
			$fileName = $request -> file('profpic');
			$ext = $fileName -> getClientOriginalExtension();
			$name = $fileName -> getClientOriginalName();
			
			if($ext = "png") {
				$im = imagecreatefrompng($fileName);
			} else {
				$im = imagecreatefromjpeg($fileName);
			}
			
			$x1 = $request -> input('x1');
			$y1 = $request -> input('y1');
			$x2 = $request -> input('x2');
			$y2 = $request -> input('y2');
			$w = $request -> input('w');
			$h = $request -> input('h');
			
			$newWidth = 315;
			$newHeight = 315;
			
			$imNew = imagecreatetruecolor($newWidth, $newHeight);
			
			imagecopyresampled($imNew, $im, 0, 0, $x1, $y1, $newWidth, $newHeight, $w, $h);
			
			
			if(!file_exists(public_path('profile/' . $request -> input('email')))) {
				mkdir(public_path('profile/' . $request -> input('email')));
			}
			
			if($ext = "png") {
				imagepng($imNew, public_path('profile/' . $request -> input('email') . "/" . $name));
			} else {
				imagejpeg($imNew, public_path('profile/' . $request -> input('email') . "/" . $name));
			}
			
			User::where('id', '=', $request -> input('id')) -> update([
					'profile_pic' => $name
			]);
			
		}
		
		$request -> session() -> flash('success', 'data successfully updated.') ;
		return back();
		
	}
	
	
	
	public function profileListPictures($type = null) {
		if($type == "image") {
			$files = Fil::where('usr_id', '=', Auth::user() -> id) -> where('path', 'LIKE', '%images%') -> get();
			
			if($files -> count() > 0) {
				return view('usrpages.profile-images', ['images' => $files]);
			} else {
				return view('usrpages.profile-noContant');
			}
		}
		
		if($type == "video") {
			$files = Fil::where('usr_id', '=', Auth::user() -> id) -> where('path', 'LIKE', '%videos%') -> get();
			
			if($files -> count() > 0) {
				return view('usrpages.profile-videos', ['videos' => $files]);
			} else {
				return view('usrpages.profile-noContant');
			}
		}
		
		if($type == "music") {
			$files = Fil::where('usr_id', '=', Auth::user() -> id) -> where('path', 'LIKE', '%music%') -> get();
			
			if($files -> count() > 0) {
				return view('usrpages.profile-music', ['music' => $files]);
			} else {
				return view('usrpages.profile-noContant');
			}
		}
		
		if($type == "document") {
			$files = Fil::where('usr_id', '=', Auth::user() -> id) -> where('path', 'LIKE', '%documents%') -> get();
			
			if($files -> count() > 0) {
				return view('usrpages.profile-documents', ['docs' => $files]);
			} else {
				return view('usrpages.profile-noContant');
			}
		}
		
	}
	
	
	public function ajaxShowHide($id = null, $value = null) {
		
		if($value == "0") {
			Fil::where('id', '=', $id) -> update([
				'shown' => 0	
			]);
			return "File has bean hidden";
		}
		
		if($value == "1") {
			Fil::where('id', '=', $id) -> update([
					'shown' => 1
			]);
			return "File has bean shown";
		}
		
		
	}
	
}














