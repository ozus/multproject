<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorit extends Model
{
    protected $table = 'favorits';
    public $timestamps = false;
    
    public function users() {
    	
    	return $this -> belongsToMany('App\User', 'users_favorits', 'fav_id', 'usr_id');
    	
    }
}
