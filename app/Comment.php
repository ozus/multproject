<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    
    public function user() {
    	
    	return $this -> belongsTo('App\User', 'usr_id');
    	
    }
    
    public function file() {
    	
    	return $this -> belongsTo('App\File', 'file_id');
    	
    }
}
