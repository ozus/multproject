<?php
use App\File;
//use Auth;
use App\Favorit;
use Carbon\Carbon;
use App\User;
use App\Mail\BirthdayMail;
use Illuminate\Support\Facades\Artisan;
//use Schema;
use Illuminate\Database\Schema\Blueprint;
//use DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'auth'], function() {
//login routes
	Route::get('/login', ['as' => 'login', 'uses' => 'LoginController@getLogin']);
	Route::post('/login', 'LoginController@postLogin');
	Route::get('logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);
	
	//register routes
	Route::get('/register', ['as' => 'register', 'uses' => 'RegisterController@getRegister']);
	Route::post('/register', 'RegisterController@postRegister');
	
	//reset password routes
	Route::get('/email', [ 'as' => 'password.reset', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
	Route::post('/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
	
	Route::get('/reset/{token?}', 'Auth\ResetPasswordController@showResetForm');
	Route::post('/reset', 'Auth\ResetPasswordController@reset');
	
	//user activation via email link
	Route::get('/activate/{id?}', 'ActivateController@activate');
});
Route::get('/', function(){
	return redirect() -> route('home');
});
Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@home']);

//uploading files
Route::get('/upload', ['as' => 'upload', 'middleware' => 'CustomAuth', 'uses' => 'UploadController@getUpload']);
Route::post('/upload', 'UploadController@postUpload');
//download files
Route::get('/download/{id?}', 'UploadController@download');
//preview files
Route::get('/show/{id?}', 'UploadController@show');
//delete files
Route::get('/delete/{id?}/{type?}', ['as' => 'file.delete', 'uses' => 'UploadController@delete']);


Route::group(['prefix' => 'user'], function() {
	//edit files 
	Route::get('/edit/{id?}', ['as' => 'edit', 'uses' => 'UserController@editFile']);
	Route::post('/edit', 'UserController@postEditFile');
	//profile
	Route::group(['prefix' => 'profile', 'middleware' => 'CustomAuth'], function() {
		Route::get('/info/{id?}', ['as' => 'user.profile', 'uses' => 'UserController@profile']);
		Route::post('/info', ['uses' => 'UserController@profilePostInfo']);
		Route::get('/media/{type?}', ['as' => 'profile.media', 'uses' => 'UserController@profileListPictures']);
		Route::get('/comments', ['as' => 'profile.info', 'uses' => 'UserController@profileListComments']);
	});
	Route::get('/showHide/{id?}/{value?}', ['as' => 'user.showhide', 'uses' => 'UserController@ajaxShowHide']);
	//add and remove favorite
	Route::get('/favorite/add/{id?}', ['as' => 'user.addFavorite', 'uses' => 'UserController@ajaxAddFavorite']);
	Route::get('favorite/remove/{id?}', ['as' => 'user.removeFavorite', 'uses' => 'UserController@ajaxRemFavorite']);
	//Greaing file
	Route::get('grade/{id?}/{type?}/{value?}', ['as' => 'user.grade', 'uses'=> 'UserController@ajaxGradeFile']);
});

//Document routs
Route::get('/document', ['as' => 'document', 'uses' => 'DocumentController@showDocs']);

//Image routes
Route::get('/image', ['as'=> 'image', 'uses' => 'ImageController@getImage']);
Route::get('/ajaxImage/{pag?}', ['as' => 'ajax.image', 'uses' => 'ImageController@ajaxImage']);
Route::get('/showImage/{id?}', ['as' => 'show.image', 'uses' => 'ImageController@showImage']);

Route::get('/imageLike/{id?}', ['as' => 'image.like', 'uses' => 'ImageController@ajaxLike']);
Route::get('/imageUnlike/{id?}', ['as' => 'image.unlike', 'uses' => 'ImageController@ajaxUnlike']);
Route::get('/imageDislike/{id?}', ['as' => 'image.dislike', 'uses' => 'ImageController@ajaxDislike']);
Route::get('/imageUndislike/{id?}', ['as' => 'image.undislike', 'uses' => 'ImageController@ajaxUndislike']);

Route::get('/image/preferences/{value?}', ['as' => 'image.preference', 'uses' => 'ImageController@ajaxPref']);

//video routes
Route::get('/video', ['as' => 'video', 'uses' => 'VideoController@getVideo']);
Route::get('/ajaxVideo/{pag?}', ['as' => 'ajax.video', 'uses' => 'VideoController@ajaxVideo']);
Route::get('/showVideo/{id?}', ['as' => 'show.video', 'uses' => 'VideoController@showVideo']);

Route::get('/videoLike/{id?}', ['as' => 'video.like', 'uses' => 'VideoController@ajaxLike']);
Route::get('/videoUnlike/{id?}', ['as' => 'video.unlike', 'uses' => 'VideoController@ajaxUnlike']);
Route::get('/videoDislike/{id?}', ['as' => 'video.dislike', 'uses' => 'VideoController@ajaxDislike']);
Route::get('/videoUndislike/{id?}', ['as' => 'video.undislike', 'uses' => 'VideoController@ajaxUndislike']);

Route::get('video/preferences/{value?}', ['as' => 'video.preference', 'uses' => 'VideoController@ajaxPref']);


//music routes
Route::get('/music', ['as' => 'music', 'uses' => 'MusicController@getMusic']);
Route::get('/ajaxMusic/{pag?}', ['as' => 'ajax.music', 'uses' => 'MusicController@ajaxMusic']);
Route::get('/showMusic/{id?}', ['as' => 'show.music', 'uses' => 'MusicController@showMusic']);

Route::get('/musicLike/{id?}', ['as' => 'music.like', 'uses' => 'MusicController@ajaxLike']);
Route::get('/musicUnlike/{id?}', ['as' => 'music.unlike', 'uses' => 'MusicController@ajaxUnlike']);
Route::get('/musicDislike/{id?}', ['as' => 'music.dislike', 'uses' => 'MusicController@ajaxDislike']);
Route::get('/musicUndislike/{id?}', ['as' => 'music.undislike', 'uses' => 'MusicController@ajaxUndislike']);

Route::get('music/preferences/{value?}', ['as' => 'music.preference', 'uses' => 'MusicController@ajaxPref']);

//comment routes
Route::group(['prefix' => 'comment'], function() {
	Route::get('/add', ['as' => 'add.comment', 'uses' => 'CommentController@addComment']);
	Route::get('/delete/{id?}', 'CommentController@deleteComment');
	Route::get('/edit/{id?}', 'CommentController@getEditComment');
	Route::post('/edit', 'CommentController@editComment');
});

//prew next pagination on show
Route::get('/next/{id?}', ['as' => 'page.pagi', 'uses' => 'MusicController@ajaxNext']);
Route::get('/prev/{id?}', ['as' => 'page.pagiPrev', 'uses' => 'MusicController@ajaxPrev']);

Route::get('/paginate/{id?}/{type?}/{role?}', ['as' => 'page.paginate', 'uses' => 'PaginationController@prevNext']);
Route::get('/auto/{id?}/{type?}', ['as' => 'page.auto', 'uses' => 'PaginationController@ajaxAuto']);

//statistic routes
Route::group(['prefix' => 'stats'], function() {
	Route::get('index', ['as' => 'stats.index', 'uses' => 'StatisticsController@index']);
	Route::get('video', ['as'=> 'stats.video', 'uses' => 'StatisticsController@video']);
	Route::get('image', ['as'=> 'stats.image', 'uses' => 'StatisticsController@image']);
	Route::get('music', ['as'=> 'stats.music', 'uses' => 'StatisticsController@music']);
	
	Route::get('/ajaxDetailsShow', ['as' => 'stats.ajaxDetailsShow', 'uses' => 'StatisticsController@ajaxDetailsShow']);
	Route::get('/ajaxDetailsHide', ['as' => 'stats.ajaxDetailsHide', 'uses' => 'StatisticsController@ajaxDetailsHide']);
});

//instant messaging routes
Route::group(['prefix' => 'message'], function() {
	Route::get('/inbox', ['as' => 'message.inbox', 'uses' => 'MessageController@inbox']);
	Route::get('/sent', ['as' => 'message.sent', 'uses' => 'MessageController@sent']);
	Route::get('/read/{id?}/{mode?}', ['as' => 'message.read', 'uses' => 'MessageController@read']);
	Route::get('/delete/{id?}/{mode?}', ['as' => 'message.delete', 'uses' => 'MessageController@ajaxDelete']);
	Route::get('/delete-single/{id?}/{mode?}', ['as' => 'message.singleDelete', 'uses' => 'MessageController@deleteSingle']);
	Route::get('/trash/{id?}/{mode?}', ['as' => 'message.toTrash', 'uses' => 'MessageController@ajaxTrash']);
	Route::get('/trash-single/{id?}/{mode?}', ['as' => 'message.singleTrash', 'uses' => 'MessageController@trashSingle']);
	Route::get('/senders/{sender?}', ['as' => 'message.getSenders', 'uses' => 'MessageController@ajaxGetSenderMessages']);
	Route::get('/recivers/{reciver?}', ['as' => 'message.getRecivers', 'uses' => 'MessageController@ajaxGetReciverMessages']);
	Route::get('/replay/{send?}', ['as' => 'message.replay', 'uses' => 'MessageController@reply']);
});



Route::get('/test', function() {
	$from = "Mario";
	$to = "Ivica";
	$title = "dummy";
	$message = "<table><tr><td><b>This is</b></td><td><i>some message</i></td></tr></table>";
	
	$array = array($from, $to, $title, $message);
	$jsonArray = json_encode($array);
	
	$input = json_decode($jsonArray);
	
	var_dump($jsonArray);
	
	$messageForSave = urlencode($message);
	
	
	if(DB::connection('mysql') -> table('messages') -> insert([
			'title' => $input[2],
			'message' => urlencode($input[3]),
			'from' => $input[0],
			'to' => $input[1],
			'send_at' => Carbon::now(),
			'recived_at' => Carbon::now(),
	])) {
		echo "Replay sent";
	} else {
		echo "Reply could not be send";
	}
	
});

Route::get('/commande', function() {
	/* $code = Artisan::call('practice:comand', [
			'param' => Auth::user() -> name,
	]); */
	
	Schema::create('practices', function(BluePrint $table) {
		$table -> increments('id');
		$table -> string('name');
	});
	
	Artisan::call('make:model', [
			'name' => 'Practice',	
	]);
	
	Artisan::call('migrate');
});






